package com.findo.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.findo.rest.model.Alumno;
import com.findo.rest.model.Curso;
import com.findo.rest.repository.CursoRepository;

@RestController
@RequestMapping("/api")

public class CursoRestController {
	
	@Autowired
	private CursoRepository cursoRepository;
	
	@PostMapping("/cursos")
	public ResponseEntity<Curso> agregarCurso(@Valid @RequestBody Curso curso) {
		curso.setId(0);
		cursoRepository.save(curso);
		
		return ResponseEntity.ok(curso);
	}
	
	@GetMapping("/cursos/{cursoId}")
	public ResponseEntity<Alumno> obtenerAlumnoMasJovenDelCurso(@PathVariable long cursoId) {
		Curso curso = cursoRepository.findById(cursoId)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

		if (curso.getAlumnos().isEmpty())
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		
		return ResponseEntity.ok(cursoRepository.alumnoMasJoven(cursoId));
	}
}
