package com.findo.rest.wrapper;

import javax.validation.constraints.NotNull;

public class InscripcionWrapper {
	@NotNull(message = "el cursoId es obligatorio.")
	Long alumnoId;
	
	@NotNull(message = "el cursoId es obligatorio.")
	Long cursoId;

	public InscripcionWrapper() {
		super();
	}

	public Long getAlumnoId() {
		return alumnoId;
	}

	public void setAlumnoId(Long alumnoId) {
		this.alumnoId = alumnoId;
	}

	public Long getCursoId() {
		return cursoId;
	}

	public void setCursoId(Long cursoId) {
		this.cursoId = cursoId;
	}
}
