package com.findo.rest.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.findo.rest.model.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long>, CursoRepositoryCustom {
	Optional<Curso> findById(long id);
}
