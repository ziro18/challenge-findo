package com.findo.rest.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import com.findo.rest.model.Alumno;
import com.findo.rest.model.Curso;

@Service
public class CursoRepositoryImpl implements CursoRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	public Alumno alumnoMasJoven(Long cursoId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		
		CriteriaQuery<Alumno> cq = cb.createQuery(Alumno.class);
		Root<Alumno> root = cq.from(Alumno.class);
		Join<Alumno, Curso> join = root.join("cursos", JoinType.INNER);
		join.on(cb.equal(join.get("id"), cursoId));
		cq.orderBy(cb.desc(root.get("fechaNacimiento")));
		TypedQuery<Alumno> q = em.createQuery(cq).setFirstResult(0).setMaxResults(1);
		
		return q.getSingleResult();
	}
	
	public List<Curso> obtenerCursosPorFecha(LocalDate fecha) {
		List<Predicate> conditionsList = new ArrayList<Predicate>();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Curso> cq = cb.createQuery(Curso.class);
		Root<Curso> root = cq.from(Curso.class);
		Predicate onStart = cb.greaterThanOrEqualTo(root.get("fechaInicio"), fecha);
		Predicate onEnd = cb.lessThanOrEqualTo(root.get("fechaFin"), fecha);
		conditionsList.add(onStart);
		conditionsList.add(onEnd);
		cq.where(conditionsList.toArray(new Predicate[]{}));

		TypedQuery<Curso> typedQuery = em.createQuery(cq);
		
		return typedQuery.getResultList();
	}
}
