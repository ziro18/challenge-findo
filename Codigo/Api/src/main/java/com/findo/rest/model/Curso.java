package com.findo.rest.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="curso")
public class Curso {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CURSO_ID")
	private long id;
	
	@Column(name="NOMBRE", nullable = false)
	@Size(max = 50, min = 3, message = "el nombre debe contener minimo 3 caracteres y maximo 50.")
	@NotNull(message = "el nombre no puede estar vacio.")
	private String nombre;
	
	@Column(name="FECHA_INICIO", nullable = false)
	@DateTimeFormat
	@NotNull(message = "la fechaInicio no puede estar vacia.")
	private LocalDate fechaInicio;
	
	@Column(name="FECHA_FIN", nullable = false)
	@DateTimeFormat
	@NotNull(message = "la fechaFin no puede estar vacia.")
	private LocalDate fechaFin;
	
	@Column(name="HORAS_SEMANALES", nullable = false)
	@Positive(message = "las horasSemanales no pueden ser negativas.")
	@NotNull(message = "las horasSemanales no pueden estar vacias.")
	private int horasSemanales;
	
	@ManyToMany(mappedBy = "cursos")
	@JsonIgnore
    private Set<Alumno> alumnos = new HashSet<>();
	

	public Curso() {
		super();
	}

	public Curso(long id, String nombre, LocalDate fechaInicio, LocalDate fechaFin, int horasSemanales) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.horasSemanales = horasSemanales;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(LocalDate fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public LocalDate getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(LocalDate fechaFin) {
		this.fechaFin = fechaFin;
	}

	public int getHorasSemanales() {
		return horasSemanales;
	}

	public void setHorasSemanales(int horasSemanales) {
		this.horasSemanales = horasSemanales;
	}

	public Set<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(Set<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaFin == null) ? 0 : fechaFin.hashCode());
		result = prime * result + ((fechaInicio == null) ? 0 : fechaInicio.hashCode());
		result = prime * result + horasSemanales;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curso other = (Curso) obj;
		if (fechaFin == null) {
			if (other.fechaFin != null)
				return false;
		} else if (!fechaFin.equals(other.fechaFin))
			return false;
		if (fechaInicio == null) {
			if (other.fechaInicio != null)
				return false;
		} else if (!fechaInicio.equals(other.fechaInicio))
			return false;
		if (horasSemanales != other.horasSemanales)
			return false;
		if (id != other.id)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
	
}
