package com.findo.rest.controller;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.findo.rest.model.Alumno;
import com.findo.rest.model.Curso;
import com.findo.rest.repository.CursoRepository;
import com.findo.rest.wrapper.ReporteWrapper;

@RestController
@RequestMapping("/api")

public class ReporteRestController {

	@Autowired
	private CursoRepository cursoRepository;
	
	@GetMapping("/reportes")
	public ResponseEntity<ReporteWrapper> obtenerReporte(@RequestParam(name = "date") String date) {
		LocalDate fecha;
		ReporteWrapper reporte = new ReporteWrapper();
		Integer edadesTotales = 0;
		
		try {
			fecha = LocalDate.parse(date);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		for (Curso curso : cursoRepository.obtenerCursosPorFecha(fecha)) {
			reporte.sumarHorasSemanalesTotales(curso.getHorasSemanales());
			reporte.sumarCantidadDeAlumnos(curso.getAlumnos().size());
			for (Alumno alumno : curso.getAlumnos()) {
				edadesTotales += Period.between(alumno.getFechaNacimiento(), LocalDate.now()).getYears();
			}
		}
		
		reporte.setEdadDeAlumnosPromedio(reporte.getCantidadDeAlumnos() != 0 ? edadesTotales / reporte.getCantidadDeAlumnos() : 0);
		
		return ResponseEntity.ok(reporte);
    }
}
