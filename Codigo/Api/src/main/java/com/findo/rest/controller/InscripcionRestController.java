package com.findo.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.findo.rest.model.Alumno;
import com.findo.rest.model.Curso;
import com.findo.rest.repository.AlumnoRepository;
import com.findo.rest.repository.CursoRepository;
import com.findo.rest.wrapper.InscripcionWrapper;

@RestController
@RequestMapping("/api")

public class InscripcionRestController {

	private static long HORAS_MAXIMAS = 20;

	@Autowired
	private AlumnoRepository alumnoRepository;
	@Autowired
	private CursoRepository cursoRepository;

	@PostMapping("/inscripciones")
	public ResponseEntity<String> agregarInscripcion(@Valid @RequestBody InscripcionWrapper inscripcionWrapper) {
		Alumno alumno = alumnoRepository.findById(inscripcionWrapper.getAlumnoId())
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		Curso curso = cursoRepository.findById(inscripcionWrapper.getCursoId())
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

		if (alumno.getHorasCursando() + curso.getHorasSemanales() > HORAS_MAXIMAS)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		alumno.setHorasCursando(alumno.getHorasCursando() + curso.getHorasSemanales());
		alumno.addCurso(curso);
		alumnoRepository.save(alumno);

		return ResponseEntity.ok("Inscripcion realizada correctamente.");
	}
}
