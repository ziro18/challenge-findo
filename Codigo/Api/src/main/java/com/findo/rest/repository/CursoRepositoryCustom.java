package com.findo.rest.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.findo.rest.model.Alumno;
import com.findo.rest.model.Curso;

@Repository
public interface CursoRepositoryCustom {
	Alumno alumnoMasJoven(Long cursoId);
	List<Curso> obtenerCursosPorFecha(LocalDate fecha);
}
