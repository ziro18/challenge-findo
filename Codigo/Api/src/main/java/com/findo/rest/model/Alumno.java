package com.findo.rest.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "alumno")
public class Alumno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ALUMNO_ID")
	private long id;

	@Column(name = "NOMBRE", nullable = false, length = 50)
	@Size(max = 50, min = 3, message = "el nombre debe contener minimo 3 caracteres y maximo 50.")
	@NotNull(message = "el nombre no puede estar vacio.")
	private String nombre;

	@Column(name = "APELLIDO", nullable = false, length = 50)
	@Size(max = 50, min = 3, message = "el apellido debe contener minimo 3 caracteres y maximo 50.")
	@NotNull(message = "el apellido no puede estar vacio.")
	private String apellido;

	@Column(name = "LIBRETA", nullable = false, length = 50)
	@Size(max = 50, min = 3, message = "la libreta debe contener minimo 3 caracteres y maximo 50.")
	@NotNull(message = "el libreta no puede estar vacio.")
	private String libreta;

	@Column(name = "FECHA_NACIMIENTO", nullable = false)
	@DateTimeFormat()
	@NotNull(message = "la fechaNacimiento no puede estar vacia.")
	private LocalDate fechaNacimiento;

	@Column(name = "HORAS_CURSANDO")
	private int horasCursando;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "alumno_curso", joinColumns = { @JoinColumn(name = "ALUMNO_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "CURSO_ID") })
	@JsonIgnore
	Set<Curso> cursos = new HashSet<>();

	public Alumno() {
		super();
	}
	
	public Alumno(long id, String nombre, String apellido, String libreta, LocalDate fechaNacimiento, int horasCursando, Set<Curso> cursos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.libreta = libreta;
		this.fechaNacimiento = fechaNacimiento;
		this.horasCursando = horasCursando;
		this.cursos = cursos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getLibreta() {
		return libreta;
	}

	public void setLibreta(String libreta) {
		this.libreta = libreta;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getHorasCursando() {
		return horasCursando;
	}

	public void setHorasCursando(int horasCursando) {
		this.horasCursando = horasCursando;
	}

	public Set<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(Set<Curso> cursos) {
		this.cursos = cursos;
	}
	
	public void addCurso(Curso curso) {
		this.cursos.add(curso);
	}
}
