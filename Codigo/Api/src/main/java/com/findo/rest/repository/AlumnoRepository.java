package com.findo.rest.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.findo.rest.model.Alumno;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Long> {
	 Optional<Alumno> findById(long id);
}
