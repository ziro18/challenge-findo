package com.findo.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.findo.rest.model.Alumno;
import com.findo.rest.repository.AlumnoRepository;

@RestController
@RequestMapping("/api")

public class AlumnoRestController {
	
	@Autowired
	private AlumnoRepository alumnoRepository;

	@PostMapping("/alumnos")
	public ResponseEntity<Alumno> agregarAlumno(@Valid @RequestBody Alumno alumno) {
		alumno.setId(0);
		alumno.setHorasCursando(0);
		alumnoRepository.save(alumno);
		
		return ResponseEntity.ok(alumno);
	}
}
