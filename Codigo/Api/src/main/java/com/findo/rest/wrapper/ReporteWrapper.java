package com.findo.rest.wrapper;

public class ReporteWrapper {
	private Integer horasSemanalesTotales;
	private Integer cantidadDeAlumnos;
	private Integer edadDeAlumnosPromedio;
	
	public ReporteWrapper() {
		super();
		this.horasSemanalesTotales = 0;
		this.cantidadDeAlumnos = 0;
		this.edadDeAlumnosPromedio = 0;
	}

	public Integer getHorasSemanalesTotales() {
		return horasSemanalesTotales;
	}

	public void setHorasSemanalesTotales(Integer horasSemanalesTotales) {
		this.horasSemanalesTotales = horasSemanalesTotales;
	}

	public Integer getCantidadDeAlumnos() {
		return cantidadDeAlumnos;
	}

	public void setCantidadDeAlumnos(Integer cantidadDeAlumnos) {
		this.cantidadDeAlumnos = cantidadDeAlumnos;
	}

	public Integer getEdadDeAlumnosPromedio() {
		return edadDeAlumnosPromedio;
	}

	public void setEdadDeAlumnosPromedio(Integer edadDeAlumnosPromedio) {
		this.edadDeAlumnosPromedio = edadDeAlumnosPromedio;
	}
	
	public void sumarHorasSemanalesTotales(Integer horasSemanalesTotales) {
		this.horasSemanalesTotales += horasSemanalesTotales;
	}
	
	public void sumarCantidadDeAlumnos(Integer cantidadDeAlumnos) {
		this.cantidadDeAlumnos += cantidadDeAlumnos;
	}
}
